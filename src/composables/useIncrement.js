import { ref } from "vue";

export const useIncrement=()=>{
    const count=ref(0);
function handleIncrement(plus) {
    count.value +=plus;
}
return {count,handleIncrement}
}