import { ref,reactive } from "vue";
import { axiosGet } from "../utils/httpClient";

export const useAxiosGet=(url,params)=>{
    const loading=ref(true);
    const data=ref([]);
    const error=ref(false)
    const fetchData = async () => {
        try {
            const res=await axiosGet(url,params);
            data.value=res;

            loading.value=false;

        } catch (error) {
         error.value=true;
         loading.value=false;
          console.log(error);
        } 
      };
      fetchData();
  
return {error,data ,loading}
}