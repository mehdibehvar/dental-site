import { createRouter, createWebHistory } from "vue-router";
import Posts from "@/views/Posts.vue";
import Home from "@/views/home/index.vue";
import BaseLayout from "@/components/layouts/BaseLayout.vue";
import PostsLayout from "../components/layouts/PostsLayout.vue";
import Users from "@/views/Users.vue";
import SingleUser from "@/views/SingleUser.vue";
import SinglePost from "@/views/SinglePost.vue";
import CreatePost from "@/views/CreatePost.vue";
import EditPost from "@/views/EditPost.vue";
import Login from "@/views/Login.vue";
import Cookies from "js-cookie";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: "/login", name: "login", component: Login },
    {
      path: "/",
      name: "baseLayout",
      component: BaseLayout,
      children: [
        { path: "", name: "home", component: Home },
        {
          path: "/users",
          name: "usersLayout",
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import("../components/layouts/UsersLayout.vue"),
          children: [
            { path: "", name: "users", component: Users },
            { path: ":id", name: "singleUser", component: SingleUser },
          ],
        },
        {
          path: "/posts",
          name: "postsLayout",
          component: PostsLayout,
          children: [
            { path: "", name: "posts", component: Posts },
            { path: ":id", name: "singlePost", component: SinglePost },
            { path: ":id", name: "editPost", component: EditPost },
          ],
        },
        { path: "/create", name: "createPost", component: CreatePost },
      ],
      beforeEnter: (to, from) => {
        if (!Cookies.get("token")) {
          return "/login";
        }
      },
    },
  
  ],
});

export default router;
