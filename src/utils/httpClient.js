import axios from "axios";
const BASE_URL="https://jsonplaceholder.typicode.com";
axios.defaults.baseURL =BASE_URL;
const instance = axios.create({
  baseURL: BASE_URL,
});

export const axiosGet = async (path, params) => {
  const options = {
    method: "GET",
    url: path,
    params,
  
  };
  const response = await instance
    .request(options)
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      console.log(error);
    });
  return response;
};
