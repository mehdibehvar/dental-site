import { defineStore } from "pinia";

export const useStyleStore=defineStore("style",{
    state:()=>({
        darkMode:false,
    }),
    actions:{
        toggleDarkMode(){
            this.darkMode=!this.darkMode;
            console.log(this.darkMode);
        }
    }
})