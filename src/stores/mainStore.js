import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import Cookies from 'js-cookie'
import axios from "axios";
import router from "@/router";

export const useMainStore = defineStore("main", {
  state: () => ({ 
    /* User */
    token:null,
    userInfo:Cookies.get("userInfo")?JSON.parse(Cookies.get("userInfo")):null
  }),
  actions: {
    loginUser(formData) {
      axios.post('https://fakestoreapi.com/auth/login',formData).then(res=>{
        if (res.data){
         this.token=res.data.token;
         Cookies.set('token', res.data.token);
         router.push("/");
        }
      }).catch(err=>console.log(err))
    },
setUser(payload){
  this.userInfo=payload;
  Cookies.set("userInfo",JSON.stringify(payload))
}
   
  },
});
